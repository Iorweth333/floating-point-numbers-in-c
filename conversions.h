//
// Created by Karol on 06.12.2018.
//

#ifndef FPGA_FLOAT_CONVERSIONS_H
#define FPGA_FLOAT_CONVERSIONS_H

#include <stdio.h>
#include <stdlib.h>
#include "globals.h"

unsigned short string2ushort(char *string,unsigned short *preceding_zeroes);
struct FPGA_float str2float(char *num_str);
char * float2str (struct FPGA_float fpga_float);
//returns dynamically allocated vector of binary numbers and it's length via pointer
bool *dec2bin(unsigned short dec, unsigned short *length);
//converts decimal number as if it was a part of a number after dot or comma
bool *frac_dec2bin(unsigned short frac, unsigned short length, unsigned short preceding_zeroes);
//this function starts counting length only after it meets first non-zero number in binary representation of the fractional number
//position where this first non-zero is fount is saved in normalisation_shift
bool *frac_dec2bin_for_less_than_zero (unsigned long long frac, unsigned short length, unsigned short *normalisation_shift, unsigned short preceding_zeroes);
unsigned long long frac_bin2dec(const bool * frac_bin, unsigned short frac_bin_length, unsigned long long * for_length_counting);
unsigned long long frac_bin2dec_2(const bool * frac_bin, unsigned short frac_bin_length, unsigned short* dec_frac_length);
unsigned short bin2dec(const bool* int_bin, unsigned short length);

char *get_int_part_of_string_number(char *num_str);
char *get_frac_part_of_string_number(char *num_str);

bool is_zero(struct FPGA_float number);
bool is_infinity (struct FPGA_float f);
bool is_NaN (struct FPGA_float f);

//returns string length, doesn't count in the terminating string
unsigned short string_length(const char *string);
//returns 1 if left > right, 0 if left == right, -1 if left < right. assumes vectors are of equal length
short compare_strings(const char *left, const char* right, unsigned short length);

//TODO: delete as soon as it's not necessary
void print_bin_vector2 (const bool * vector, unsigned short length);

#endif //FPGA_FLOAT_CONVERSIONS_H
