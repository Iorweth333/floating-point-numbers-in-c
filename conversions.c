//
// Created by Karol on 06.12.2018.
//

#include "conversions.h"

//converts c-string to unsigned short. iterates starting from the end. does not check if the really string contains number.
unsigned short string2ushort(char *string, unsigned short *preceding_zeroes) {
    *preceding_zeroes = 0;
    if (string == NULL) {
        return 0;
    }
    unsigned short res = 0;
    unsigned short mult = 1;
    unsigned short iter = string_length(string) - (unsigned short) 1;    // -1 cause zero-based indexing
    bool finished = false;
    while (!finished) {
        (*preceding_zeroes)++;
        if (string[iter] != '0') *preceding_zeroes = 0;
        res = res + ((unsigned short) (string[iter] - 48) * mult);
        mult = mult * (unsigned short) 10;
        if (iter == 0) finished = true;
        else iter--;
    }
    return res;
}

struct FPGA_float str2float(char *num_str) {
    struct FPGA_float result;
    result.exponent = calloc(g_exponent_length, sizeof(bool));
    result.mantissa = malloc(g_mantissa_length * sizeof(bool));

    if (num_str[0] == '-') result.sign = true;
    else result.sign = false;
    if (compare_strings(num_str, "0", 2) == 0) {  //zero is a special value
        for (int i = 0; i < g_exponent_length; i++) result.exponent[i] = false;
        for (int i = 0; i < g_mantissa_length; i++) result.mantissa[i] = false;
        return result;
    }

    char *int_part_str = get_int_part_of_string_number(num_str);
    char *frac_part_str = get_frac_part_of_string_number(num_str);
    unsigned short preceding_zeroes;
    unsigned short int_part = string2ushort(int_part_str,
                                            &preceding_zeroes);   //preceding zeroes are meaningless for int_part
    unsigned short frac_part;
    unsigned short int_part_bin_length;
    bool *int_part_bin;
    short normalisation_shift;
    unsigned short frac_part_bin_length;
    bool *frac_part_bin;

    if (int_part > 0) {
        frac_part = string2ushort(frac_part_str, &preceding_zeroes);
        int_part_bin = dec2bin(int_part, &int_part_bin_length);
        normalisation_shift = int_part_bin_length - (unsigned short) 1;
        frac_part_bin_length = g_mantissa_length - normalisation_shift;
        frac_part_bin = frac_dec2bin(frac_part, frac_part_bin_length, preceding_zeroes);
        //normalisation
        unsigned short i = 1;                           //starting from 1 cause' first "1" is always omitted
        while (i < int_part_bin_length && i < g_mantissa_length + 1) {
            result.mantissa[i - 1] = int_part_bin[i];
            i++;
        }
        i--;    //i pointed where the fractional part should start, but that was including the omitted "1"- that's why it must be decremented
        for (int j = 0; j < frac_part_bin_length && i < g_mantissa_length; j++) {
            result.mantissa[i] = frac_part_bin[j];
            i++;
        }
    } else {   //int_part == 0, it can't be < 0
        frac_part = string2ushort(frac_part_str, &preceding_zeroes);
        if (frac_part == 0) {    //0 or 0.0
            normalisation_shift = 0;    //important for processing exponent- it will be equal to bias
            for (int i = 0; i < g_mantissa_length; i++) result.mantissa[i] = false;
        } else {
            unsigned short shift;
            frac_part_bin = frac_dec2bin_for_less_than_zero(frac_part, g_mantissa_length, &shift, preceding_zeroes);
            normalisation_shift = (unsigned short) 0 - shift;
            for (int i = 0; i < g_mantissa_length; i++) result.mantissa[i] = frac_part_bin[i];
        }
    }

    unsigned short bias = 1;
    for (int j = 0; j < g_exponent_length - 1; j++) bias *= 2;
    bias -= 1;

    unsigned short exponent_bin_length;
    bool *exponent2be = dec2bin(bias + normalisation_shift, &exponent_bin_length);

    if (exponent_bin_length > g_exponent_length) {
        free(result.exponent);
        free(result.mantissa);
        result.exponent = NULL;
        result.mantissa = NULL;
        printf("Exponent capacity not long enough to hold such a number. Minimal exponent length necessary: %hu",
               exponent_bin_length);
        return result;
    } else {
        for (int j = g_exponent_length - 1; exponent_bin_length > 0; j--) {
            result.exponent[j] = exponent2be[exponent_bin_length -
                                             1];    //exponent_bin_length will now serve as an iterator
            exponent_bin_length--;
        }   //the rest of the exponent is zeroed by calloc
    }
    return result;
}


char *float2str(struct FPGA_float fpga_float) {
    char *result;
    if (is_zero(fpga_float)) {
        result = malloc(2 * sizeof(char));
        result[0] = '0';
        result[1] = '\0';
        return result;
    }
    if (is_infinity(fpga_float)) {
        //result = malloc(4 * sizeof(char));
        result = "Inf\0";
        return result;
    }
    if (is_NaN (fpga_float)){
        result = "NaN\0";
        return result;
    }

    unsigned short bias = 1;
    for (int j = 0; j < g_exponent_length - 1; j++) bias *= 2;
    bias -= 1;
    short exponent = bin2dec(fpga_float.exponent, g_exponent_length) - bias;
    unsigned short absolute_of_exponent = 0;    //this is only needed when exponent is negative
    bool *denormalised_mantissa_int_part;
    int i = 0;
    if (exponent >= 0) {
        denormalised_mantissa_int_part = calloc((unsigned short) exponent + (unsigned short) 1, sizeof(bool));
        denormalised_mantissa_int_part[0] = true;   //the "1" which is omitted
        while (i < exponent && i < g_mantissa_length) {
            i++;
            denormalised_mantissa_int_part[i] = fpga_float.mantissa[i - 1];
        }
    } else {
        denormalised_mantissa_int_part = calloc(1, sizeof(bool));
        denormalised_mantissa_int_part[0] = false;   //if exponent is negative, int part equals 0
        absolute_of_exponent = (unsigned short) 0 - exponent;
    }


    if (debug) {
        printf("float to string. mantissa and exponent:\n");
        print_bin_vector2(fpga_float.mantissa, g_mantissa_length);
        printf("\n");
        print_bin_vector2(fpga_float.exponent, g_exponent_length);
        printf("\n");
    }

    // i now points the element after which there is a comma (in binary representation). Pay attention to the shift due to omitted "1"
    short bin_frac_part_len = g_mantissa_length -
                              exponent;    //exponent tells about how far has comma or dot been moved. this assignment is also valid for negative exponent
    unsigned short unsigned_bin_frac_part_len;
    if (bin_frac_part_len < 0) unsigned_bin_frac_part_len = 0;
    else unsigned_bin_frac_part_len = (unsigned short) bin_frac_part_len;
    bool *denormalised_mantissa_frac_part = calloc(unsigned_bin_frac_part_len, sizeof(bool));
    unsigned long long for_length_counting;
    unsigned short dec_frac_length;
    unsigned long long frac_part;
    unsigned long long tmp;
    unsigned short str_len;
    int dot_position;

    if (exponent >= 0) {
        int j = 0;
        while (i < g_mantissa_length) {
            denormalised_mantissa_frac_part[j] = fpga_float.mantissa[i];
            i++;
            j++;
        }
        unsigned short int_part = bin2dec(denormalised_mantissa_int_part, exponent + (unsigned short) 1);

        //frac_part = frac_bin2dec(denormalised_mantissa_frac_part, unsigned_bin_frac_part_len, &for_length_counting);
        frac_part = frac_bin2dec_2(denormalised_mantissa_frac_part, unsigned_bin_frac_part_len, &dec_frac_length);

        unsigned short int_part_str_len = 0;
        str_len = 0;

        tmp = int_part;
        while (tmp > 0) {
            tmp /= 10;
            str_len++;
            int_part_str_len++;
        }
        str_len += dec_frac_length;
        if (frac_part == 0) str_len += 1;
        else str_len += 2; //for the comma and terminating character




        int first_loop_starts_with;
        if (fpga_float.sign == true) {   //negative number
            str_len++;
            result = malloc(str_len * sizeof(char));
            result[0] = '-';
            first_loop_starts_with = int_part_str_len;
            if (unsigned_bin_frac_part_len != 0) result[int_part_str_len + 1] = '.';     //remember 0-based indexing
            dot_position = int_part_str_len + 1;
        } else {                           //positive number
            first_loop_starts_with = int_part_str_len - 1;
            result = malloc(str_len * sizeof(char));
            if (unsigned_bin_frac_part_len != 0) result[int_part_str_len] = '.';     //remember 0-based indexing
            dot_position = int_part_str_len;
        }


        for (int k = first_loop_starts_with; k >= 0 && int_part > 0; k--) {  //first loop, writes integer part
            result[k] = (char) ((int_part % 10) + 48);      //48 = code for 0 in ASCII
            int_part /= 10;
        }
        for (int k = str_len - 2; k > dot_position; k--) {    //minus 2 because of terminating sign and 0-based indexing
            result[k] = (char) ((frac_part % 10) + 48);     //48 = code for 0 in ASCII
            frac_part /= 10;
        }
        result[str_len - 1] = '\0';  //adding terminating character
        return result;
    } else {    //negative exponent
        i = 0;
        while (i < absolute_of_exponent - 1) {         //minus 1 because of "1" that is always omitted
            denormalised_mantissa_frac_part[i] = false;
            i++;
        }
        denormalised_mantissa_frac_part[i] = true; //adding 1 that is always omitted
        i++;
        int j = 0;
        while (j < g_mantissa_length) {
            denormalised_mantissa_frac_part[i] = fpga_float.mantissa[j];
            i++;
            j++;
        }


        //frac_part = frac_bin2dec(denormalised_mantissa_frac_part, unsigned_bin_frac_part_len, &for_length_counting);
        frac_part = frac_bin2dec_2(denormalised_mantissa_frac_part, unsigned_bin_frac_part_len, &dec_frac_length);
        if (fpga_float.sign == true) {    //negative number
            str_len = 4;    //for "-0." and terminating character
        } else {
            str_len = 3;        //for "0." and terminating character
        }

        str_len += dec_frac_length;

        result = malloc(str_len * sizeof(char));

        if (fpga_float.sign == true) {    //putting minus
            result[0] = '-';
            result[1] = '0';
            result[2] = '.';
            dot_position = 2;
        } else {
            result[0] = '0';
            result[1] = '.';
            dot_position = 1;
        }

        for (int k = str_len - 2; k > dot_position; k--) {    //minus 2 because of terminating sign and 0-based indexing
            result[k] = (char) ((frac_part % 10) + 48);     //48 = code for 0 in ASCII
            frac_part /= 10;
        }
        result[str_len - 1] = '\0';  //adding terminating character
        return result;
    }
}

bool *dec2bin(unsigned short dec, unsigned short *length) {
    bool *bin;
    if (dec == 0) {
        *length = 1;
        bin = malloc(1 * sizeof(bool));
        bin[0] = false;
        return bin;
    }

    unsigned short tmp = dec;
    unsigned short bin_array_len = 0;
    while (tmp > 0) {
        tmp /= 2;
        bin_array_len++;    //need to know length before malloc
    }

    *length = bin_array_len;
    bin = malloc(bin_array_len * sizeof(bool));

    for (int i = bin_array_len - 1; i >= 0; i--) {
        unsigned short mod = dec % (unsigned short) 2;
        if (mod == 1) bin[i] = true;
        else bin[i] = false;
        dec /= 2;
    }
    return bin;
}

unsigned short bin2dec(const bool *int_bin, unsigned short length) {
    unsigned short factor = 1;
    unsigned short result = 0;

    for (int i = length - 1; i >= 0; i--) {
        if (int_bin[i] == true) result += factor;
        factor *= 2;
    }
    return result;
}

bool *frac_dec2bin(unsigned short frac, unsigned short length, unsigned short preceding_zeroes) {
    bool *frac_bin = calloc(length, sizeof(bool));
    unsigned short divider = 1;
    unsigned short tmp = frac;
    while (tmp > 0) {
        tmp /= 10;
        divider *= 10;
    }
    while (preceding_zeroes > 0) {
        divider *= 10;
        preceding_zeroes--;
    }

    for (int i = 0; i < length; i++) {
        frac *= 2;
        frac_bin[i] = (bool) (frac / divider);
        frac -= frac_bin[i] * divider;
    }
    return frac_bin;
}

//this function starts counting length only after it meets first non-zero number in binary representation of the fractional number
//position where this first non-zero is fount is saved in normalisation_shift
bool *
frac_dec2bin_for_less_than_zero(unsigned long long frac, unsigned short length, unsigned short *normalisation_shift,
                                unsigned short preceding_zeroes) {
    bool *frac_bin = calloc(length, sizeof(bool));
    if (frac == 0) return frac_bin;
    unsigned long long divider = 1;
    unsigned long long tmp = frac;
    while (tmp > 0) {
        tmp /= 10;
        divider *= 10;
    }
    while (preceding_zeroes > 0) {
        divider *= 10;
        preceding_zeroes--;
    }
    *normalisation_shift = 0;
    unsigned short i = 0;
    int frac_bin_iterator = 0;
    bool next_bin_number;
    while (frac_bin_iterator < length) {
        i++;
        frac *= 2;
        next_bin_number = (bool) (frac / divider);
        frac -= next_bin_number * divider;
        if (*normalisation_shift == 0) {    //first non-zero hasn't been spotted yet
            if (next_bin_number == true) {  //spots first non-zero number
                *normalisation_shift = i;   //the '1' found just now is not saved in frac_bin array- it's omitted
            }
        } else {
            frac_bin[frac_bin_iterator] = next_bin_number;
            frac_bin_iterator++;
        }
        if (frac == 0)
            return frac_bin; //in this case there is no point in iterating any further. the rest of an array is zeroed by calloc
    }
    return frac_bin;
}

//obsolete:
unsigned long long
frac_bin2dec(const bool *frac_bin, unsigned short frac_bin_length, unsigned long long *for_length_counting) {
    unsigned long long factor = 1;
    unsigned long long divider = 2;
    unsigned long long result = 0;
    unsigned long long tmp = factor;
    for (int i = 0; i < frac_bin_length; i++) {
        result *= 10;
        factor *= 10;
        if (((factor / 10) != tmp) || factor < tmp) {
            printf("Non critical error: Conversion from fractional binary to decimal overloaded unsigned long long range!\n");
            printf("Last known correct result variable: %llu \n", result);
            return result;
        } else tmp = factor;

        if (frac_bin[i] == true) result += factor / divider;
        divider *= 2;
    }
    *for_length_counting = factor;
    return result;
}

//new implementation of above. still nonperfect- very often variables overflow
unsigned long long
frac_bin2dec_2(const bool *frac_bin, unsigned short frac_bin_length, unsigned short *dec_frac_length) {
    bool isZero = true;
    for (int i = 0; i < frac_bin_length && isZero; i++) {
        if (frac_bin[i] != false) isZero = false;
    }
    if (isZero) {
        *(dec_frac_length) = 0;
        return 0;
    }

    unsigned long long factor = 1;
    unsigned long long divider = 2;
    unsigned long long result = 0;
    unsigned long long tmp = factor;
    bool first_non_zero_already_found = false;
    *dec_frac_length = 0;
    for (int i = 0; i < frac_bin_length; i++) {
        result *= 10;
        factor *= 10;
        if (((factor / 10) != tmp) || factor < tmp) {
            printf("Non critical error: Conversion from fractional binary to decimal overloaded unsigned long long range!\n");
            printf("Last known correct result variable: %llu \n", result);
            return result;
        } else tmp = factor;

        if (frac_bin[i] == true) {
            result += factor / divider;
            first_non_zero_already_found = true;
        }
        divider *= 2;
        (*dec_frac_length)++;
    }
    return result;
}


char *get_int_part_of_string_number(char *num_str) {
    unsigned short input_length = string_length(num_str);
    unsigned short j = 0;
    unsigned short i = 0;
    int int_part_len;
    if (num_str[0] == '-') {
        i = 1;
        int_part_len = input_length;    //no need to add 1 cause there is a minus in input- terminating sign will take it's count
    } else
        int_part_len = input_length + 1;

    char *int_part = malloc(int_part_len * sizeof(char)); //upper estimate of length
    while (i < input_length && num_str[i] != '.' && num_str[i] != ',') {
        int_part[j] = num_str[i];
        i++;
        j++;
    }
    //i points out where dot or comma is
    //int_part contains integer part without minus character
    if (i == input_length) {
        int_part[j] = '\0';
        return int_part;      //in this case scenario input is integer
    }

    char *result;                                  //vector of preciseful length (including terminating sign)
    if (num_str[0] == '-') {                         //eg. for -123.56 i=5
        result = malloc((i) * sizeof(char));
        for (int k = 0; k < i; k++) {
            result[k] = int_part[k];
        }
    } else {                                           //eg. 112.45 i=4
        result = malloc((i + 1) * sizeof(char));
        for (int k = 0; k < i; k++) {
            result[k] = int_part[k];
        }
    }
    result[i] = '\0';  //terminating sign
    free(int_part);
    int_part = NULL;
    return result;
}

char *get_frac_part_of_string_number(char *num_str) {
    unsigned short input_length = string_length(num_str);
    unsigned short i = 0;
    while (i < input_length && num_str[i] != '.' && num_str[i] != ',') i++;
    //i points out where dot or comma is
    if (i == input_length) return NULL; // number is integer, no fractional part
    else {
        i++;    //to omit . or ,
        char *frac_part = malloc(
                (input_length - i) * sizeof(char));       //eg. 1,1 i=1 len = 3 frac-PART LEN = 1 + 1 for terminator
        unsigned short j = 0;
        while (i < input_length) {
            frac_part[j] = num_str[i];
            j++;
            i++;
        }
        frac_part[j] = '\0';
        return frac_part;
    }
}

bool is_zero(struct FPGA_float number) {
    for (int i = 0; i < g_mantissa_length; i++) if (number.mantissa[i] != false) return false;
    for (int i = 0; i < g_exponent_length; i++) if (number.exponent[i] != false) return false;
    return true;
}

//return string length. does not count end of line sign or terminating character
unsigned short string_length(const char *string) {    //maybe some exception handling for exceeding ushort range?
    if (string == NULL) return 0;
    unsigned short length = 0;
    unsigned short iter = 0;
    while (string[iter] != '\0' && string[iter] != '\n') {
        length++;
        iter++;
    }
    return length;
}

//returns 1 if left > right, 0 if left == right, -1 if left < right. assumes vectors are of equal length
short compare_strings(const char *left, const char *right, unsigned short length) {
    for (int i = 0; i < length; i++) {
        if (left[i] > right[i]) return 1;
        if (left[i] < right[i]) return -1;
    }
    return 0;
}

//TODO: delete as soon as it's not necessary

void print_bin_vector2(const bool *vector, unsigned short length) {
    for (unsigned short i = 0; i < length; i++) {
        if (vector[i] == true) printf("1");
        else printf("0");
    }
}


bool is_infinity(struct FPGA_float f) {
    for (int i = 0; i < g_mantissa_length; i++) {
        if (f.mantissa[i] != false) return false;
    }
    for (int j = 0; j < g_exponent_length; j++) {
        if (f.exponent[j] != true) return false;
    }
    return true;
}

bool is_NaN (struct FPGA_float f){
    bool answer = false;
    for (int j = 0; j < g_exponent_length; j++) {
        if (f.exponent[j] != true) return false;    //is exponent maximum?
    }
    for (int i = 0; i < g_mantissa_length && answer == false; i++) {
        if (f.mantissa[i] == true) answer = true;   //is mantissa non-zero?
    }
    return answer;
}