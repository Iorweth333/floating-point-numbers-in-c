#include <stdio.h>      //how does cmd input and output work on FPGA?
#include <stdlib.h>     //and dynamic memory allocation?
#include "conversions.h"
#include "globals.h"

bool *add_binaries(bool *left, bool *right, unsigned short length);
void add_binaries_in_multiplication(bool *temporary_mantissa, const bool *added_mantissa_no_omission,
                                    const unsigned short added_mantissa_no_omission_length, unsigned short shift);

bool *subtract_binaries(bool *left, bool *right, unsigned short length);
bool *
multiply_binaries(const bool *left_mantissa_no_omission, const bool *right_mantissa_no_omission, unsigned short length);

short compare_binaries(const bool *left, const bool *right, unsigned short length);

struct FPGA_float add_floats(struct FPGA_float left, struct FPGA_float right);
struct FPGA_float subtract_floats(struct FPGA_float left, struct FPGA_float right);
struct FPGA_float multiply_floats(struct FPGA_float left, struct FPGA_float right);
struct FPGA_float divide_floats(struct FPGA_float left, struct FPGA_float right);
struct FPGA_float sqrt_float(struct FPGA_float number);

bool *right_shift(const bool *array, unsigned short how_many, unsigned short length);
bool *left_shift(const bool *array, unsigned short how_many, unsigned short length);

bool is_number(const char *string);

struct FPGA_float infinity();
struct FPGA_float zero();
struct FPGA_float NaN();

void print_bin_vector(const bool *vector, unsigned short length);


int main(int argc, char *argv[]) {
    debug = false;
    if (debug) setbuf(stdout, 0);
    const unsigned short numbers_str_len = 10;
    if (argc != 3) {
        printf("This is a very simple calculator. Maximal length of numbers: %hu\n", numbers_str_len);
        printf("Please provide input and output files' paths/names. First two lines should contain: \n"
               "length of exponent and then length of mantissa. \n"
               "Then arithmetical operations to be calculated: \n"
               "a number, operator and the second number, everything in separate lines.\n"
               "The program assumes correctness of the input\n"
               "Content of the output file will be overwritten!\n"
               "\n");
        return 1;
    }

    FILE *input;
    FILE *output;

    input = fopen(argv[1], "r");
    if (input == NULL) {
        printf("Something is not right with input file.\n");
        return 1;
    }
    output = fopen(argv[2], "w");

    char *line = malloc(numbers_str_len * sizeof(char));
    unsigned short preceding_zeroes;
    fscanf(input, "%s", line);
    g_exponent_length = string2ushort(line, &preceding_zeroes);
    fscanf(input, "%s", line);
    g_mantissa_length = string2ushort(line, &preceding_zeroes);

    g_bias = 1;
    for (int j = 0; j < g_exponent_length - 1; j++) g_bias *= 2;
    g_bias -= 1;


    struct FPGA_float left_number;
    struct FPGA_float right_number;
    struct FPGA_float result;

    char operator;
    char line_feed;
    char *left_number_str;
    char *right_number_str;
    char *result_str;

    while (feof(input) == 0) {       //non-zero if eof indicator is set
        fscanf(input, "%s", line);
        left_number = str2float(line);
        fscanf(input, "%c", &line_feed);

        fscanf(input, "%c", &operator);
        fscanf(input, "%c", &line_feed);

        if (operator == 's'){
            result = sqrt_float(left_number);
            left_number_str = float2str(left_number);
            result_str = float2str(result);
            fprintf(output, "sqrt(%s) = %s \n", left_number_str, result_str);

        }
        else{
            fscanf(input, "%s", line);
            right_number = str2float(line);
            fscanf(input, "%c", &line_feed);


            left_number_str = float2str(left_number);
            right_number_str = float2str(right_number);


            switch (operator) {
                case '+':
                    result = add_floats(left_number, right_number);
                    break;
                case '-':
                    result = subtract_floats(left_number, right_number);
                    break;
                case '*':
                    result = multiply_floats(left_number, right_number);
                    break;
                case '/':
                    result = divide_floats(left_number, right_number);
                    break;
                default:
                    printf("Unknown operator: %c\n", operator); //and just omit
                    free(left_number_str);  //ofc need to clean up due to "continue" below
                    free(right_number_str);
                    continue;
            }

            result_str = float2str(result);
            fprintf(output, "%s %c %s = %s \n", left_number_str, operator, right_number_str, result_str);
            free(right_number_str);
            right_number_str = NULL;
        }

        free(left_number_str);
        left_number_str = NULL;
        free(result_str);
        result_str = NULL;
        free(result.mantissa);
        free(result.exponent);
    }

    fclose(input);
    fclose(output);
    return 0;
}


//HUGE TODO: what if one (or even both) exponents are negative?
//adds two float numbers assuming they are positive
struct FPGA_float add_floats(struct FPGA_float left, struct FPGA_float right) {
    struct FPGA_float result;
    result.sign = false;
    result.mantissa = malloc(g_mantissa_length * sizeof(bool));
    result.exponent = calloc(g_exponent_length, sizeof(bool));

    if (is_zero(left)) {
        for (int i = 0; i < g_mantissa_length; i++) result.mantissa[i] = right.mantissa[i];
        for (int i = 0; i < g_exponent_length; i++) result.exponent[i] = right.exponent[i];
        result.sign = right.sign;
        return result;
    }
    if (is_zero(right)) {
        for (int i = 0; i < g_mantissa_length; i++) result.mantissa[i] = left.mantissa[i];
        for (int i = 0; i < g_exponent_length; i++) result.exponent[i] = left.exponent[i];
        result.sign = left.sign;
        return result;
    }

    bool *left_mantissa_no_omission = malloc((g_mantissa_length + 1) * sizeof(bool));
    bool *right_mantissa_no_omission = malloc((g_mantissa_length + 1) * sizeof(bool));
    left_mantissa_no_omission[0] = true;
    right_mantissa_no_omission[0] = true;
    for (int i = 1; i < g_mantissa_length + 1; i++) {
        left_mantissa_no_omission[i] = left.mantissa[i - 1];
        right_mantissa_no_omission[i] = right.mantissa[i - 1];
    }

    if (debug) {
        printf("adding floats. left mantissa, left with no omission, right, right with no omission\n");
        print_bin_vector(left.mantissa, g_mantissa_length);
        printf("\n");
        print_bin_vector(left_mantissa_no_omission, g_mantissa_length + (unsigned short) 1);
        printf("\n");
        print_bin_vector(right.mantissa, g_mantissa_length);
        printf("\n");
        print_bin_vector(right_mantissa_no_omission, g_mantissa_length + (unsigned short) 1);
    }


    unsigned short left_exponent = bin2dec(left.exponent, g_exponent_length);
    unsigned short right_exponent = bin2dec(right.exponent, g_exponent_length);
    unsigned short res_exponent_dec = 0;
    bool *res_mantissa;
    bool *res_exponent;
    unsigned short res_exponent_len;

    //processing mantissa:
    if (left_exponent == right_exponent) {
        res_mantissa = add_binaries(left_mantissa_no_omission, right_mantissa_no_omission,
                                    g_mantissa_length + (unsigned short) 1);
        res_exponent_dec = left_exponent;
    } else {
        bool *shifted_mantissa;
        if (left_exponent > right_exponent) {
            unsigned short len_difference = left_exponent - right_exponent;
            shifted_mantissa = right_shift(right_mantissa_no_omission, len_difference,
                                           g_mantissa_length + (unsigned short) 1);
            res_mantissa = add_binaries(left_mantissa_no_omission, shifted_mantissa,
                                        g_mantissa_length + (unsigned short) 1);
            res_exponent_dec = left_exponent;
        } else {
            unsigned short len_difference = right_exponent - left_exponent;
            shifted_mantissa = right_shift(left_mantissa_no_omission, len_difference,
                                           g_mantissa_length + (unsigned short) 1);
            res_mantissa = add_binaries(right_mantissa_no_omission, shifted_mantissa,
                                        g_mantissa_length + (unsigned short) 1);
            res_exponent_dec = right_exponent;
        }
        free(shifted_mantissa);
        shifted_mantissa = NULL;
    }
    if (res_mantissa[0] == true) {                       // this means mantissa is not normalised- we must shift
        for (int i = 1; i < g_mantissa_length + 1; i++) {
            result.mantissa[i - 1] = res_mantissa[i];
        }
        //now modify exponent- must add 1
        res_exponent_dec++;

    } else {
        for (int i = 2; i < g_mantissa_length + 2; i++) {
            result.mantissa[i - 2] = res_mantissa[i];
        }
    }
    //processing exponent
    res_exponent = dec2bin(res_exponent_dec, &res_exponent_len);
    short i = g_exponent_length - (short) 1;    //cannot be unsigned short because while loop wouldn't end
    short j = res_exponent_len - (short) 1;
    if (res_exponent_len > g_exponent_length) printf("Exponent exceeded given range!\n");
    while (j >= 0) {
        result.exponent[i] = res_exponent[j];
        i--;
        j--;    //exponent is zeroed by calloc
    }

    free(left_mantissa_no_omission);
    free(right_mantissa_no_omission);
    left_mantissa_no_omission = NULL;
    right_mantissa_no_omission = NULL;

    return result;
}

//HUGE TODO: what if one (or even both) exponents are negative?
//subtracts two float numbers assuming they're positive
struct FPGA_float subtract_floats(struct FPGA_float left, struct FPGA_float right) {
    struct FPGA_float result;
    result.mantissa = malloc(g_mantissa_length * sizeof(bool));
    result.exponent = calloc(g_exponent_length, sizeof(bool));

    if (is_zero(left)) {
        for (int i = 0; i < g_mantissa_length; i++) result.mantissa[i] = right.mantissa[i];
        for (int i = 0; i < g_exponent_length; i++) result.exponent[i] = right.exponent[i];
        result.sign = (bool) !right.sign;  //please notice negation
        return result;
    }
    if (is_zero(right)) {
        for (int i = 0; i < g_mantissa_length; i++) result.mantissa[i] = left.mantissa[i];
        for (int i = 0; i < g_exponent_length; i++) result.exponent[i] = left.exponent[i];
        result.sign = left.sign;
        return result;
    }

    //add ones that are usually omitted
    bool *left_mantissa_no_omission = malloc((g_mantissa_length + 1) * sizeof(bool));
    bool *right_mantissa_no_omission = malloc((g_mantissa_length + 1) * sizeof(bool));
    left_mantissa_no_omission[0] = true;
    right_mantissa_no_omission[0] = true;
    for (int i = 1; i < g_mantissa_length + 1; i++) {
        left_mantissa_no_omission[i] = left.mantissa[i - 1];
        right_mantissa_no_omission[i] = right.mantissa[i - 1];
    }

    if (debug) {
        printf("subtracting floats. left mantissa, left with no omission, right, right with no omission:\n");
        print_bin_vector(left.mantissa, g_mantissa_length);
        printf("\n");
        print_bin_vector(left_mantissa_no_omission, g_mantissa_length + (unsigned short) 1);
        printf("\n");
        print_bin_vector(right.mantissa, g_mantissa_length);
        printf("\n");
        print_bin_vector(right_mantissa_no_omission, g_mantissa_length + (unsigned short) 1);
    }


    unsigned short left_exponent = bin2dec(left.exponent, g_exponent_length);
    unsigned short right_exponent = bin2dec(right.exponent, g_exponent_length);
    unsigned short res_exponent_dec = 0;
    bool *res_mantissa;
    bool *shifted_mantissa;
    bool *res_exponent;

    //processing mantissa:
    if (left_exponent == right_exponent) {
        short cmp = compare_binaries(left_mantissa_no_omission, right_mantissa_no_omission,
                                     g_mantissa_length + (unsigned short) 1);
        switch (cmp) {
            case 1:     //left greater than right
                res_mantissa = subtract_binaries(left_mantissa_no_omission, right_mantissa_no_omission,
                                                 g_mantissa_length + (unsigned short) 1);
                result.sign = false;
                break;
            case -1:    //right is greater than left
                res_mantissa = subtract_binaries(right_mantissa_no_omission, left_mantissa_no_omission,
                                                 g_mantissa_length + (unsigned short) 1);
                result.sign = true;
                break;
            default:   //numbers are exactly the same- result is zero
                for (int i = 0; i < g_mantissa_length; i++)
                    result.mantissa[i] = false;
                for (int i = 0; i < g_exponent_length; i++)
                    result.exponent[i] = false;
                result.sign = false;
                return result;
        }
        res_exponent_dec = left_exponent;
    } else {
        if (left_exponent > right_exponent) {
            unsigned short len_difference = left_exponent - right_exponent;
            shifted_mantissa = right_shift(right_mantissa_no_omission, len_difference,
                                           g_mantissa_length + (unsigned short) 1);
            res_mantissa = subtract_binaries(left_mantissa_no_omission, shifted_mantissa,
                                             g_mantissa_length + (unsigned short) 1);
            res_exponent_dec = left_exponent;
            result.sign = false;
        } else {
            unsigned short len_difference = right_exponent - left_exponent;
            shifted_mantissa = right_shift(left_mantissa_no_omission, len_difference,
                                           g_mantissa_length + (unsigned short) 1);
            res_mantissa = subtract_binaries(right_mantissa_no_omission, shifted_mantissa,
                                             g_mantissa_length + (unsigned short) 1);
            res_exponent_dec = right_exponent;
            result.sign = true;
        }
        free(shifted_mantissa);
        shifted_mantissa = NULL;
    }
    if (res_mantissa[0] == false) {                       // this means mantissa is not normalised- we must shift
        unsigned short shift = 1;
        int iter = 1;
        while (iter < (g_mantissa_length + 1) && res_mantissa[iter] == false) {
            iter++;
            shift++;
        }
        shifted_mantissa = left_shift(res_mantissa, shift, g_mantissa_length + (unsigned short) 1);

        for (int i = 1; i < g_mantissa_length + 1; i++) {
            result.mantissa[i - 1] = shifted_mantissa[i];
        }
        free(shifted_mantissa);
        shifted_mantissa = NULL;

        //now modify exponent- must subtract shift
        res_exponent_dec -= shift;

    } else {
        for (int i = 1; i < g_mantissa_length + 1; i++) {
            result.mantissa[i - 1] = res_mantissa[i];
        }
    }



    //processing exponent
    unsigned short res_exponent_len;
    res_exponent = dec2bin(res_exponent_dec, &res_exponent_len);
    short i = g_exponent_length - (short) 1;    //cannot be unsigned short because "while" loop wouldn't end
    short j = res_exponent_len - (short) 1;
    if (res_exponent_len > g_exponent_length) printf("Exponent exceeded given range!\n");
    while (j >= 0) {
        result.exponent[i] = res_exponent[j];
        i--;
        j--;    //exponent is zeroed by calloc
    }

    free(left_mantissa_no_omission);
    free(right_mantissa_no_omission);
    free(res_mantissa);
    left_mantissa_no_omission = NULL;
    right_mantissa_no_omission = NULL;
    res_mantissa = NULL;

    return result;


}

struct FPGA_float multiply_floats(struct FPGA_float left, struct FPGA_float right) {
    if (is_zero(left) || is_zero(right)) {
        return zero();
    }
    struct FPGA_float result;
    if ((left.sign == true && right.sign == true) || (left.sign == false && right.sign == false)) result.sign = false;
    else result.sign = true;
    result.mantissa = malloc(g_mantissa_length * sizeof(bool));
    result.exponent = calloc(g_exponent_length, sizeof(bool));


    bool *left_mantissa_no_omission = malloc((g_mantissa_length + 1) * sizeof(bool));
    bool *right_mantissa_no_omission = malloc((g_mantissa_length + 1) * sizeof(bool));
    left_mantissa_no_omission[0] = true;
    right_mantissa_no_omission[0] = true;
    for (int i = 1; i < g_mantissa_length + 1; i++) {
        left_mantissa_no_omission[i] = left.mantissa[i - 1];
        right_mantissa_no_omission[i] = right.mantissa[i - 1];
    }

    if (debug) {
        printf("multiplying floats. left mantissa, left with no omission, right, right no omission:\n");
        print_bin_vector(left.mantissa, g_mantissa_length);
        printf("\n");
        print_bin_vector(left_mantissa_no_omission, g_mantissa_length + (unsigned short) 1);
        printf("\n");
        print_bin_vector(right.mantissa, g_mantissa_length);
        printf("\n");
        print_bin_vector(right_mantissa_no_omission, g_mantissa_length + (unsigned short) 1);
    }

    bool *temporary_mantissa = multiply_binaries(left_mantissa_no_omission, right_mantissa_no_omission, (g_mantissa_length + (unsigned short) 1));
    //temporary mantissa is always sized like this: (g_mantissa_length + 1) * 2

    bool increment_exponent = false;
    int temp_mantissa_iter = 2;         //in case temp_mantissa starts like this 01... and the first one must be omitted
    if (temporary_mantissa[0] == true) {    //in case it starts like 1... and similar omission
        increment_exponent = true;         //plus it means shift so exponent must be incremented
        temp_mantissa_iter = 1;
    }
    for (int i = 0; i < g_mantissa_length; i++) {
        result.mantissa[i] = temporary_mantissa[temp_mantissa_iter];
        temp_mantissa_iter++;
    }



    //processing exponent
    unsigned short bias = g_bias;

    short left_exponent_dec =
            bin2dec(left.exponent, g_exponent_length) - bias;     //todo: check if there is no over/under-flow
    short right_exponent_dec = bin2dec(right.exponent, g_exponent_length) - bias;
    unsigned short res_exponent_dec = left_exponent_dec + right_exponent_dec + bias;
    if (increment_exponent) res_exponent_dec++;

    unsigned short final_exponent_length;
    bool *final_exponent = dec2bin(res_exponent_dec, &final_exponent_length);
    if (final_exponent_length > g_exponent_length) {
        printf("Warning! Due to multiplication exponent became too long. Maximal exponent will be used instead\n");
        for (int i = 0; i < g_exponent_length - 1; i++) result.exponent[i] = true;
        result.exponent[g_exponent_length - 1] = false;     //because exponent full of ones is reserved for NaN
    } else {
        int iter = g_exponent_length - final_exponent_length;
        for (int i = 0; i < final_exponent_length; i++) {
            result.exponent[iter] = final_exponent[i];
            iter++;
        }
    }

    return result;
}

struct FPGA_float divide_floats(struct FPGA_float left, struct FPGA_float right) {
    if (is_zero(right)) {
        printf("ERROR! DIVISION BY ZERO!\n");
        return infinity();
    }
    if (is_zero(left)) {
        return zero();
    }
    struct FPGA_float result;
    if ((left.sign == true && right.sign == true) || (left.sign == false && right.sign == false)) result.sign = false;
    else result.sign = true;
    result.mantissa = calloc(g_mantissa_length, sizeof(bool));
    result.exponent = calloc(g_exponent_length, sizeof(bool));

    //border cases

    //normal cases:
    //process mantissa

    bool *dividend = malloc((g_mantissa_length + 2) * sizeof(bool));
    bool *divisor = malloc((g_mantissa_length + 2) * sizeof(bool));
    //plus 2 because omission and that specific subtraction
    bool *tmp_mantissa = malloc((g_mantissa_length + 1) * sizeof(bool));
    //plus just one because there might be shift necessary
    // not two because shift_needed tells about first bit
    divisor[0] = false; //just fulfilling a gap
    divisor[1] = true;  //omitted 1
    for (int i = 2; i < g_mantissa_length + 2; i++) divisor[i] = right.mantissa[i - 2];

    bool shift_needed = false;
    short comparison = compare_binaries(left.mantissa, right.mantissa, g_mantissa_length);
    //compare_binaries returns 1 if left > right, 0 if left == right, -1 if left < right

    if (comparison != 0) {   //if mantissa's are equal, result mantissa will stay zeroed (by calloc)
        if (comparison == -1) { //right mantissa is greater
            shift_needed = true;
            dividend[g_mantissa_length + 1] = false;    //just fulfilling a gap (this is basically shifting left by one)
            dividend[0] = true;                         //omitted 1
            for (int i = 1; i < g_mantissa_length + 1; i++) dividend[i] = left.mantissa[i - 1];
        } else {
            dividend[0] = false;
            dividend[1] = true;                         //omitted 1
            for (int i = 2; i < g_mantissa_length + 2; i++) dividend[i] = left.mantissa[i - 2];
        }

        bool *tmp;
        tmp = subtract_binaries(dividend, divisor, g_mantissa_length + (unsigned short) 2);
        free(dividend);
        dividend = tmp;

        for (int j = 0; j < g_mantissa_length + 1; j++) {
            comparison = compare_binaries(dividend, divisor, g_mantissa_length + (unsigned short) 2);
            if (comparison == -1) {  //divisor is greater, subtraction impossible, need to shift
                tmp = left_shift(dividend, 1, g_mantissa_length + (unsigned short) 2);
                free(dividend);
                dividend = tmp;
                tmp_mantissa[j] = false;
            } else {
                tmp = subtract_binaries(dividend, divisor, g_mantissa_length + (unsigned short) 2);
                free(dividend);
                dividend = tmp;
                tmp_mantissa[j] = true;
            }
        }
        for (int k = 0; k < g_mantissa_length; k++) result.mantissa[k] = tmp_mantissa[k + 1];
        //need to omit one due to former left shift by one

    }

    //process exponent:
    unsigned short bias = g_bias;

    short left_exponent_dec = bin2dec(left.exponent, g_exponent_length) - bias;
    short right_exponent_dec = bin2dec(right.exponent, g_exponent_length) - bias;
    unsigned short biased_res_exponent_dec = left_exponent_dec - right_exponent_dec + bias;
    //re-bias because above biases cancel each other
    if (shift_needed) biased_res_exponent_dec--;

    unsigned short final_exponent_length;
    bool *final_exponent = dec2bin(biased_res_exponent_dec, &final_exponent_length);
    if (final_exponent_length > g_exponent_length) {
        printf("Warning! Due to division exponent became too long. Maximal exponent will be used instead\n");
        for (int i = 0; i < g_exponent_length - 1; i++) result.exponent[i] = true;
        result.exponent[g_exponent_length - 1] = false;     //because exponent full of ones is reserved for NaN
    } else {
        int iter = g_exponent_length - final_exponent_length;
        for (int i = 0; i < final_exponent_length; i++) {
            result.exponent[iter] = final_exponent[i];
            iter++;
        }
    }

    return result;
}

struct FPGA_float sqrt_float(struct FPGA_float number) {
    if (is_zero(number)) return zero();
    if (number.sign == true) return NaN();

    struct FPGA_float result;
    result.mantissa = calloc(g_mantissa_length, sizeof(bool));
    result.exponent = calloc(g_exponent_length, sizeof(bool));

    unsigned short bias = g_bias;
    short exponent_dec = bin2dec(number.exponent, g_exponent_length) - bias;
    short res_exponent_dec;
    bool* mantissa_no_omission;
    bool* temporary_mantissa;
    unsigned short mantissa_no_omission_len;
    unsigned short iter;
    int start_copying_from;

    bool exponent_even;
    if (exponent_dec % 2 == 0) exponent_even = true;
    else exponent_even = false;

    mantissa_no_omission_len = g_mantissa_length + (unsigned short) 2;
    mantissa_no_omission = calloc(mantissa_no_omission_len, sizeof(bool));

    if (exponent_even) {
        res_exponent_dec = exponent_dec / (short) 2;
        mantissa_no_omission[1] = true;
        for (int i = 0; i < g_mantissa_length; i++) mantissa_no_omission[i + 2] = number.mantissa[i];
    } else {
        res_exponent_dec = (exponent_dec - (short) 1) / (short) 2;
        mantissa_no_omission[0] = true;
        for (int i = 0; i < g_mantissa_length; i++) mantissa_no_omission[i + 1] = number.mantissa[i];
        //gap is filled by calloc
    }


    unsigned short temporary_mantissa_len = g_mantissa_length + (unsigned short) 1;
    temporary_mantissa = calloc(temporary_mantissa_len, sizeof(bool));
    temporary_mantissa[0] = true;
    iter = 0;
    start_copying_from = 1;

    if (debug) {
        printf("calculating square root. input mantissa and input mantissa after considering value of exponent:\n");
        print_bin_vector(number.mantissa, g_mantissa_length);
        printf("\n");
        print_bin_vector(mantissa_no_omission, mantissa_no_omission_len);
        printf("\n");
    }

    bool *powered_mantissa = multiply_binaries(temporary_mantissa, temporary_mantissa, temporary_mantissa_len);
    short comparison = compare_binaries(powered_mantissa, mantissa_no_omission, mantissa_no_omission_len);
    //comparison returns 1 if powered > expected, 0 if equal, -1 if powered < expected.
    while (comparison != 0 && iter < temporary_mantissa_len - 1){   //-1 because it's incremented before it's used
        if (comparison > 0) temporary_mantissa[iter] = false;

        iter++;
        temporary_mantissa[iter] = true;
        free(powered_mantissa);
        powered_mantissa = multiply_binaries(temporary_mantissa, temporary_mantissa, temporary_mantissa_len);
        comparison = compare_binaries(powered_mantissa, mantissa_no_omission, mantissa_no_omission_len);
    }

    for (int i=0; i< g_mantissa_length; i++) {
        result.mantissa[i] = temporary_mantissa[start_copying_from];
        start_copying_from++;
    }

    //processing exponent

    unsigned short final_exponent_length;
    unsigned short biased_res_exponent_dec = res_exponent_dec + bias;
    bool *final_exponent = dec2bin(biased_res_exponent_dec, &final_exponent_length);
    if (final_exponent_length > g_exponent_length) {
        printf("Warning! Due to multiplication exponent became too long. Maximal exponent will be used instead\n");
        for (int i = 0; i < g_exponent_length - 1; i++) result.exponent[i] = true;
        result.exponent[g_exponent_length - 1] = false;     //because exponent full of ones is reserved for NaN
    } else {
        iter = g_exponent_length - final_exponent_length;
        for (int i = 0; i < final_exponent_length; i++) {
            result.exponent[iter] = final_exponent[i];
            iter++;
        }
    }

    return result;


}

struct FPGA_float infinity() {
    struct FPGA_float f;
    f.exponent = malloc(g_exponent_length * sizeof(bool));
    f.mantissa = malloc(g_mantissa_length * sizeof(bool));

    for (int i = 0; i < g_mantissa_length; i++) {
        f.mantissa[i] = false;
    }
    for (int j = 0; j < g_exponent_length; j++) {
        f.exponent[j] = true;
    }
    return f;
}

struct FPGA_float zero() {
    struct FPGA_float zero;
    zero.sign = false;
    zero.exponent = calloc(g_exponent_length, sizeof(bool));
    zero.mantissa = calloc(g_mantissa_length, sizeof(bool));
    return zero;
}

struct FPGA_float NaN() {    //signaling NaN: maximum exponent, non-zero mantissa with 0 as first element
    struct FPGA_float nan;
    nan.sign = false;
    nan.exponent = malloc(g_exponent_length * sizeof(bool));
    nan.mantissa = malloc(g_mantissa_length * sizeof(bool));

    for (int i = 0; i < g_exponent_length; i++) nan.exponent[i] = true;
    for (int i = 0; i < g_mantissa_length; i++) nan.mantissa[i] = false;
    nan.mantissa[g_mantissa_length - 1] = true;
    return nan;
};


//computes sum of two binary numbers. output array is always one bit longer
bool *add_binaries(bool *left, bool *right, unsigned short length) {
    bool *res = malloc((length + 1) * sizeof(bool));
    unsigned short iter = length - (unsigned short) 1;  //to iterate from last
    bool carry = false;
    bool finished = false;
    bool skip;

    if (debug) {
        printf("\nbefore binary addition: left and then right:\n ");
        print_bin_vector(left, length);
        printf("\n ");
        print_bin_vector(right, length);
    }

    while (!finished) {
        skip = false;
        if (((left[iter] == true && right[iter] != true) || (left[iter] != true && right[iter] == true)) &&
            // (1 + 0 or 0 + 1) and carry 0
            carry == false) {
            res[iter + 1] = true;
            skip = true;
        }
        if (!skip &&    // (1 + 0 or 0 + 1) and carry 1
            ((left[iter] == true && right[iter] != true) || (left[iter] != true && right[iter] == true)) &&
            carry == true) {
            res[iter + 1] = false;
            carry = true;       //redundant, but it's ok
            skip = true;
        }
        if (!skip && left[iter] == true && right[iter] == true && carry == false) {     //1 + 1 and carry 0
            res[iter + 1] = false;
            carry = true;
            skip = true;
        }
        if (!skip && left[iter] == true && right[iter] == true && carry == true) {        //1 + 1 and carry 1
            res[iter + 1] = true;
            carry = true;
            skip = true;
        }
        if (!skip && left[iter] == false && right[iter] == false && carry == false) {       // 0 + 0 and carry 0
            res[iter + 1] = false;
            skip = true;
        }
        if (!skip && left[iter] == false && right[iter] == false && carry == true) {        // 0 + 0 and carry 1
            res[iter + 1] = true;
            carry = false;
        }


        if (iter == 0) finished = true;
        else iter = iter - (unsigned short) 1;
    }
    if (carry == true) res[0] = true;
    else res[0] = false;


    if (debug) {
        printf(" after addition: result:\n");
        print_bin_vector(res, length + (unsigned short) 1);
        printf("\n");
    }

    return res;
}
//adds "added_mantissa_no_omission" to "temporary_mantissa". result is stored in the latter
void add_binaries_in_multiplication(bool *temporary_mantissa, const bool *added_mantissa_no_omission,
                                    const unsigned short added_mantissa_no_omission_length, unsigned short shift) {
    unsigned short temporary_mantissa_length = added_mantissa_no_omission_length * (unsigned short) 2;
    unsigned short main_iter =
            added_mantissa_no_omission_length - (unsigned short) 1;    //to iterate from last element of left_m....
    unsigned short temporary_mantissa_iter = temporary_mantissa_length - shift - (unsigned short) 1;
    bool carry = false;
    bool finished = false;
    bool skip;

    if (debug) {
        printf("\nbefore binary addition in multiplication (shift: %u): temporary and then left:\n ", shift);
        print_bin_vector(temporary_mantissa, temporary_mantissa_length);
        printf("\n ");
        for(int i=0; i< added_mantissa_no_omission_length - shift;i++) printf(" ");
        print_bin_vector(added_mantissa_no_omission, added_mantissa_no_omission_length);
    }

    while (!finished) {
        skip = false;
        if (((temporary_mantissa[temporary_mantissa_iter] == true && added_mantissa_no_omission[main_iter] != true) ||
             (temporary_mantissa[temporary_mantissa_iter] != true && added_mantissa_no_omission[main_iter] == true)) &&
            // (1 + 0 or 0 + 1) and carry 0
            carry == false) {
            temporary_mantissa[temporary_mantissa_iter] = true;
            skip = true;
        }
        if (!skip &&    // (1 + 0 or 0 + 1) and carry 1
            ((temporary_mantissa[temporary_mantissa_iter] == true && added_mantissa_no_omission[main_iter] != true) ||
             (temporary_mantissa[temporary_mantissa_iter] != true && added_mantissa_no_omission[main_iter] == true)) &&
            carry == true) {
            temporary_mantissa[temporary_mantissa_iter] = false;
            carry = true;       //redundant, but it's ok
            skip = true;
        }
        if (!skip && temporary_mantissa[temporary_mantissa_iter] == true &&
            added_mantissa_no_omission[main_iter] == true && carry == false) {     //1 + 1 and carry 0
            temporary_mantissa[temporary_mantissa_iter] = false;
            carry = true;
            skip = true;
        }
        if (!skip && temporary_mantissa[temporary_mantissa_iter] == true &&
            added_mantissa_no_omission[main_iter] == true && carry == true) {        //1 + 1 and carry 1
            temporary_mantissa[temporary_mantissa_iter] = true;
            carry = true;
            skip = true;
        }
        if (!skip && temporary_mantissa[temporary_mantissa_iter] == false &&
            added_mantissa_no_omission[main_iter] == false && carry == false) {       // 0 + 0 and carry 0
            temporary_mantissa[temporary_mantissa_iter] = false;
            skip = true;
        }
        if (!skip && temporary_mantissa[temporary_mantissa_iter] == false &&
            added_mantissa_no_omission[main_iter] == false && carry == true) {        // 0 + 0 and carry 1
            temporary_mantissa[temporary_mantissa_iter] = true;
            carry = false;
        }


        if (main_iter == 0) finished = true;
        else {
            main_iter--;
        }
        temporary_mantissa_iter--;
    }
    if (carry == true) temporary_mantissa[temporary_mantissa_iter] = true;
    //as far as I can tell this is safe and never exceed boundary
    else temporary_mantissa[temporary_mantissa_iter] = false;


    if (debug) {
        printf(" after addition in multiplication: result:\n ");
        print_bin_vector(temporary_mantissa, temporary_mantissa_length);
        printf("\n");
    }
}

//computes subtraction of two binary numbers. left should be thegreater one
bool *subtract_binaries(bool *left, bool *right, unsigned short length) {
    bool *res = malloc(length * sizeof(bool));
    unsigned short iter = length - (unsigned short) 1;  //to iterate from last
    bool carry = false;
    bool finished = false;
    bool skip;

    if (debug) {
        printf("\nbefore binary subtraction: left and then right:\n ");
        print_bin_vector(left, length);
        printf("\n ");
        print_bin_vector(right, length);
    }

    while (!finished) {
        skip = false;   // (0 - 0 and carry 0) or (1 - 0 and carry 1) or (1 - 1 and carry 0)
        if ((left[iter] == false && right[iter] == false && carry == false) ||
            (left[iter] == true && right[iter] == false && carry == true) ||
            (left[iter] == true && right[iter] == true && carry == false)) {
            res[iter] = false;
            carry = false;
            skip = true;
        }
        if (!skip &&    // (0 - 1 carry 0) or (0 - 0 carry 1) or (1 - 1  carry 1)
            ((left[iter] == false && right[iter] == true && carry == false) ||
             (left[iter] == false && right[iter] == false && carry == true) ||
             (left[iter] == true && right[iter] == true && carry == true))) {
            res[iter] = true;
            carry = true;
            skip = true;
        }
        if (!skip && left[iter] == true && right[iter] == false && carry == false) {     //1 - 0 carry 0
            res[iter] = true;
            carry = false;
            skip = true;
        }
        if (!skip && left[iter] == false && right[iter] == true && carry == true) {        //0 - 1 carry 1
            res[iter] = false;
            carry = true;
            skip = true;
        }

        if (iter == 0) finished = true;
        else iter = iter - (unsigned short) 1;
    }

    if (debug) {
        printf(" after binary subtraction: result:\n ");
        print_bin_vector(res, length);
        printf("\n");
    }
    return res;
}

bool *multiply_binaries(const bool *left_mantissa_no_omission, const bool *right_mantissa_no_omission, unsigned short length) {
    unsigned short temporary_mantissa_size = length * (unsigned short) 2;
    //it can be maximally twice as big.
    bool *temporary_mantissa = calloc(temporary_mantissa_size, sizeof(bool));

    unsigned short shift = 0;
    for (int iter = g_mantissa_length; iter >= 0; iter--) {
        //again because of the omission: g_mantissa_length without -1
        if (right_mantissa_no_omission[iter] == true) {
            add_binaries_in_multiplication(temporary_mantissa, left_mantissa_no_omission,
                                           g_mantissa_length + (unsigned short) 1, shift);
        }   //else unnecessary- it would add zeroes
        shift++;
    }
    return temporary_mantissa;
}

//returns 1 if left > right, 0 if left == right, -1 if left < right. assumes vectors are of equal length
short compare_binaries(const bool *left, const bool *right, const unsigned short length) {
    for (int i = 0; i < length; i++) {
        if (left[i] == true && right[i] == false) return 1;
        if (left[i] == false && right[i] == true) return -1;
    }
    return 0;
}

bool is_number(const char *string) {   //TODO: what about E-notation?
    unsigned short iter = 0;
    while (string[iter] != '\0') {
        if (!((string[iter] >= '0' && string[iter] <= '9') || string[iter] == ',' || string[iter] == '.'))
            return false;
        iter++;
    }
    return true;
}


//shift right - dividing
bool *right_shift(const bool *array, unsigned short how_many, unsigned short length) {
    bool *result = calloc(length, sizeof(bool));
    bool data_loss = false;
    unsigned short iter = length - how_many;
    while (!data_loss && iter < length) {
        if (array[iter] == true) {
            printf("\nWarning! There might be a significant data loss during left shift\n");
            data_loss = true;
        }
        iter++;
    }

    for (int j = how_many; j < length; j++) {
        result[j] = array[j - how_many];
    }
    return result;
}

//shift left -> multiplying
bool *left_shift(const bool *array, unsigned short how_many, unsigned short length) {
    bool *result = calloc(length, sizeof(bool));
    bool data_loss = false;
    unsigned short iter = 0;
    while (!data_loss && iter < how_many) {
        if (array[iter] == true) {
            printf("Warning! There might be a significant data loss during left shift");
            data_loss = true;
        }
        iter++;
    }


    for (int j = 0; j < length - how_many; j++) {
        result[j] = array[j + how_many];
    }
    return result;
}

void print_bin_vector(const bool *vector, unsigned short length) {
    for (unsigned short i = 0; i < length; i++) {
        if (vector[i] == true) printf("1");
        else printf("0");
    }
}