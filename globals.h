//
// Created by Karol on 07.12.2018.
//

#ifndef FPGA_FLOAT_GLOBALS_H
#define FPGA_FLOAT_GLOBALS_H

unsigned short g_exponent_length;
unsigned short g_mantissa_length;
unsigned short g_bias;

typedef enum { false, true } bool;

bool debug;

struct FPGA_float {
    bool sign;
    bool *exponent;
    bool *mantissa;
};

#endif //FPGA_FLOAT_GLOBALS_H
